# REPTE ABP

### FASE 1

En aquest nou projecte treballaré de manera individual per tal d'aprendre i interioritzar millor tots els conceptes necessaris.

Com al projecte NoLeftovers, les funcionalitats seran les mateixes: afegir, modificar i esborrar productes de la base de dades, el mateix per usuaris, tot i que d'aquests en trobarem dos tipus: consumidors (la majoria) i venedors.

El link al git del projecte és el següent: [repteABP](https://gitlab.com/isx41747980/repteabp)

En aquest git l'estructura serà la següent:

- Evitar sempre que es pugui treballar en la branca master.
- Crear per cada funcionalitat una nova branca.
- Un cop tot funcioni correctament a una branca, fer merge amb master.